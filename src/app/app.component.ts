import { Component,ElementRef , ViewChild } from '@angular/core';
import { UserService } from './services/user.service'; 
import { Router } from '@angular/router';
import { LoginService } from './services/login.service';
import { NotificationService } from './services/notification.service';
import { FormControl } from '@angular/forms';
import { Gtag } from 'angular-gtag';

declare let FB: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  @ViewChild('messenger') private scrollContainer : ElementRef;
  userStatus;
  show = false;
  messages = [];
  msg = new FormControl('');
  id;
  email;
  name;

  constructor(private user : UserService , private router : Router , private loginService : LoginService , private notif : NotificationService,gtag: Gtag){
    this.email = this.user.email();
    this.name = this.user.getName();
    this.userStatus = this.user.getUserStatus();
    this.notif.getId().subscribe(id=>{
      if(id){
        this.id=id;
        this.show = true;
        this.messages = this.notif.getMessagesById(id);
      }
      else this.show = false;
    });
    this.user.getUserStatus().subscribe(val=>{
      if(val){
        this.notif.initializeSocket();
        this.notif.getMatches();
      }
    });
    this.notif.getAllMessages().subscribe(val=>{
      if(val){
        this.messages = val;
      }
    });
  }

  ngOnInit(){
    FB.init({
      appId: '2106085152943268',
      autoLogAppEvents: true,
      cookie: true,
      xfbml: true,
      version: 'v3.0'
    });
    FB.getLoginStatus((response: any) => {
      if (response.status === 'connected') {
        let authResponse = response.authResponse;
        FB.api(`/me?fields=name,email,picture,first_name,last_name`, (fbUser: any) => {
            let obj = {
              idToken : authResponse.userID,
              first_name : fbUser.first_name,
              last_name : fbUser.last_name,
              email : fbUser.email, 
            };
            let  b = this.loginService.facebookAuth(obj)
            .subscribe(res=>{
              if(res['status']==200){
                this.user.setUserStatus(true);
                this.router.navigateByUrl('home');
                b.unsubscribe();
              }
            });
        });
      }
    },{scope : 'public_profile,email'});  
    this.scrollToBottom();
  }

  close(){
    this.notif.setId('');
  }

  send(){
    console.log(this.name);
    let nw={
      content : this.msg.value,
      time : new Date(),
      email : this.email,
      name : this.name,
    }
    // this.messages.push(nw);
    this.notif.sendMessage(nw,this.id);
    this.msg.reset('');
  }

  scrollToBottom(): void {
    try {
        this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;
    } catch(err) { }                 
  }

}
