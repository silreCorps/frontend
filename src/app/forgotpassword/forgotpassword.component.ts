import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  email = new FormControl('',[Validators.required,Validators.email]);
  disp = '';
  err = '';
  constructor(private login : LoginService) { }

  ngOnInit() {
  }

  onSubmit(){
    if(this.email.valid){ 
      this.login.forgotPassword(this.email.value)
      .subscribe(res=>{
        if(res['status'] == 200){
          this.disp = res['result'];
        }
        else this.err = res['result'];
      });
    }
  }
}
