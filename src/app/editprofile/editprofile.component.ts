import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
 
  Form : FormGroup;
  profile_pic: File = null;
  pic = '';
  profile = {
    first_name: '',
    last_name :'',
    location : '',
    favourite_sport : '',
    gender : '',
    date_of_birth : '',
    profile_pic : ''
  };
  err = '';
  email = '';
  formErr = '';

  constructor(private fb : FormBuilder , private user : UserService , private router : Router ) {
    this.user.getUserStatus().subscribe(value => {
      if(!value) {
        this.router.navigateByUrl('/');
      }
    });
    this.email = this.user.getEmail();
    if(this.email){
      let a = this.user.getProfile(this.email)
      .subscribe(res =>{
        if(res['status'] == 200){
          this.profile = res['result'];
          this.pic = this.profile.profile_pic;
        }
        a.unsubscribe()
      });
    }
    this.createForm();
  }

  ngOnInit() {
  }

  createForm(){
    this.Form = this.fb.group({
      first_name : '',
      location : '',
      favourite_sport : '',
      last_name : '',
      gender : '',
      date_of_birth : ''
    });
  }

  onSubmit(){
    let obj = {
      first_name : this.Form.value.first_name ? this.Form.value.first_name : this.profile.first_name,
      last_name : this.Form.value.last_name ? this.Form.value.last_name : this.profile.last_name,
      location : this.Form.value.location ? this.Form.value.location : this.profile.location,
      favourite_sport :  this.Form.value.favourite_sport ? this.Form.value.favourite_sport : this.profile.favourite_sport,
      gender : this.Form.value.gender ? this.Form.value.gender : this.profile.gender,
      date_of_birth : this.Form.value.date_of_birth ? this.Form.value.date_of_birth : this.profile.date_of_birth,
    }
    let a = this.user.updateProfileDetails(obj)
    .subscribe(res=>{
      if(res['status']==200){
        this.router.navigateByUrl('/profile/'+this.email);
      }
      else{
        this.err = res['result'];
      }
      a.unsubscribe();
    });
    if(this.profile_pic){
    let b = this.user.updateProfilePicture(this.profile_pic)
    .subscribe(res=>{
      if(res['status']==200){
        // do something
      }
      else{
        // do something
      }
      b.unsubscribe();
    });
    }
  }
  
  handleFileInput(files: FileList) {
    this.profile_pic = files.item(0);
    //console.log(files);
    var reader = new FileReader();

    reader.readAsDataURL(files[0]); // read file as data url

    reader.onload = (event) => { 
      // called once readAsDataURL is completed
      this.pic = reader.result.toString();
    }
  }
}
