import { Component, OnInit,ElementRef,ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventsService } from '../services/events.service';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { NotificationService } from '../services/notification.service';
import {Observable , fromEvent} from 'rxjs';
import {debounceTime, distinctUntilChanged, map , filter} from 'rxjs/operators';

const sports = ['Badminton','Tennis','Cricket','Volleyball','Basketball','Football'
,'Table Tennis','Pool','Chess','Kabaddi','Kho Kho','Hide and Seek','Hockey','Baseball',];

let friends = [];

@Component({
  selector: 'app-create-new-event',
  templateUrl: './create-new-event.component.html',
  styleUrls: ['./create-new-event.component.css']
})
export class CreateNewEventComponent implements OnInit {
  
  @ViewChild('inviteinput') private inputEl : ElementRef;

  eventForm : FormGroup;
  err ='' ;
  email='';
  loc = [28.38,77.12];
  minDate = {
    day : 0,
    month : 0,
    year : 0,
  };
  date = '';
  public model : any;
  spinners = false;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 1 ? [] 
        : sports.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  invite = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => {
      let index = term.lastIndexOf(" ");
      if(index >= 0){
        let word = term.substr(index,term.length);
        console.log(word);
        return word.length < 1 ? [] : friends.filter(v => v.name.toLowerCase().indexOf(word.toLowerCase()) > -1).slice(0, 10)
      }
      else
        return  term.length < 1 ? [] : friends.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10)
    })
  );
  invites = [];

  constructor(public fb : FormBuilder ,public es : EventsService,private notif : NotificationService , public router : Router,private user : UserService) { 
    
    this.user.getUserStatus().subscribe(value => {
      if(!value) {
        this.router.navigateByUrl('/');
      }
    });
    
    this.email = this.user.getEmail();
    
    let date = new Date;
    this.minDate.day = date.getDay();
    this.minDate.month = date.getMonth()+1;
    this.minDate.year = date.getFullYear();

    this.user.getfriends().subscribe(res=>{
      if(res['status']==200){
        friends = res['result'];
      }
    });
    
  }

  ngOnInit() {
    this.createForm();
    //this.getloc();
  }

  createForm(){
    this.eventForm = this.fb.group({
      date : ['',Validators.required],
      time : ['',Validators.required],
      sport : ['',Validators.required],
      address : ['',Validators.required],
      note : '',
      contact : '',
    })
  }
  
  selected($e){
    $e.preventDefault();
    friends.splice(friends.indexOf($e.item),1);
    this.invites.push($e.item);
    this.inputEl.nativeElement.value = '';
  }

  cancel(friend){
    this.invites.splice(this.invites.indexOf(friend),1);
    friends.push(friend);
  }

  onEventSubmit(){
    this.err = '';

    let date = this.eventForm.value.date;
    let time = this.eventForm.value.time;
    
    let obj = {
      date_time : new Date(date['year'],date['month']-1,date['day'],time['hour'],time['minute'],time['second']),
      sport : this.eventForm.value.sport,
      address : this.eventForm.value.address,
      contact : this.eventForm.value.contact,
      participants : [],
      note : this.eventForm.value.note,
      organizer : this.email,
    }

    if(this.eventForm.valid && obj.date_time > new Date())
      this.es.createNewEvent(obj).subscribe(res => {

        if(res['status'] == 200){
          this.notif.getMatches();
          //this.es.sendInvites({invites : this.invites, id : res['result']});
          this.router.navigateByUrl('/events/'+res['result']);
        }
        else {
          this.err = res['result'];
        }
      });

    else if( obj.date_time < new Date() ) this.err = 'please enter a valid date';
    
    else this.err = 'Please fill all fields'
  
  }

  getloc(){
    if(navigator.geolocation){
      var options ={
        enableHighAccuracy : true
      }
      navigator.geolocation.getCurrentPosition(
        (pos)=> {
          this.loc = [pos.coords.latitude,pos.coords.longitude];   
        },
        (err)=>{
        },
        options);
    }
    else {
    }
  }
}
