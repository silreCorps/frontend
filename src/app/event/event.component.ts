import { Component, OnInit,ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../services/events.service';
import { UserService } from '../services/user.service';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { Meta } from '@angular/platform-browser';
import { NotificationService } from '../services/notification.service';

declare let gapi : any;
declare let FB : any;

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  event = {
    _id : '',
    sport : '',
    date_time :'',
    address : '',
    note : '',
    participiants : [],
  };
  disp = false;
  err = "";
  isLoading = true;
  email='';
  show = true;
  fbIcon = faFacebookSquare;
  url;
  loading = false;
  GoogleAuth :any;
  loggedIn : boolean = false;
  modal : NgbModalRef;
  userStatus;
  
  constructor(private route : ActivatedRoute , private es : EventsService , private user : UserService , private router : Router , private notif : NotificationService) {
    this.userStatus = this.user.getUserStatus().subscribe(res=>{
      if(!res) this.router.navigateByUrl('/home');
    });
    this.email = this.user.getEmail();
    this.route.params.subscribe(param =>{
      let id = param['id'];
      this.es.getEvent(id)
      .subscribe(res => {
        if(res['status']==200){
          this.event = res['result'];
          new Date(this.event.date_time) > new Date() ? this.disp = true : this.disp = false;
          this.event['participants'].some(ele => {
            if(ele.email == this.email){
              this.show = false;
              return false;
            } 
            else{
              return false;
            }
          });
          let r= res['result'];
          this.url='https://socioplay.in/#/events/'+r._id;
        }
        else{
          this.err = res['result'];
        }
        this.isLoading = false;
      })
    });
  }

  ngOnInit() {
  }

  join(){
    this.es.join(this.event['_id'])
    .subscribe(res=>{
      if(res['status']==200){
        this.notif.getMatches();
        this.notif.join(this.event['_id']);
        this.es.getEvent(this.event['_id'])
        .subscribe(res => {
          if(res['status']==200){
            this.event = res['result'];
            this.show = false;
          }
          else{
            this.err = res['result'];
          }
          this.isLoading = false;
        })
  
      }
      else{
        this.err=res['result'];
      }
    });
  }

  leave(){
    this.es.leave(this.event['_id'])
    .subscribe(res=>{
      if(res['status']==200){
        this.notif.getMatches();
        this.es.getEvent(this.event['_id'])
        .subscribe(res => {
          if(res['status']==200){
            this.router.navigateByUrl('/home');  
          }
          else{
            this.err = res['result'];
          }
          this.isLoading = false;
        })  
      }
      else{
        this.err = res['result'];
      }
    });
  }

  edit(){
    this.router.navigateByUrl('/edit/'+this.event._id);
  }
  msg(id){
    this.notif.setId(id);
    this.router.navigateByUrl('/messenger');
  }
  mesag(id){
    this.notif.setId(id);
  }
}
