import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventsService } from '../services/events.service';
import { NotificationService } from '../services/notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-edit-match',
  templateUrl: './edit-match.component.html',
  styleUrls: ['./edit-match.component.css']
})
export class EditMatchComponent implements OnInit {

  eventForm : FormGroup;
  err ='';
  email='';
  warn = '';
  id="";
  placeholder={
    sport : '',
    note : '',
    contact : '',
    address : '' 
  }
  spinners = false;
  minDate = {
    day : 0,
    month : 0,
    year : 0,
  };

  constructor(private route : ActivatedRoute ,public fb : FormBuilder,public es : EventsService,private notif : NotificationService , public router : Router,private user : UserService) { 
    this.route.params.subscribe(param =>{
      let id = param['id'];
      this.id=id;
      this.createForm();
      this.es.getEvent(id)
      .subscribe(res => {
        //console.log(res);
        if(res['status']==200){
          let r= res['result'];
          let d = new Date(res['result'].date_time);
          let date = {
            day : d.getDate(),
            month : d.getUTCMonth()+1,
            year : d.getFullYear(),
          };
          let time = {
            hour : d.getHours(),
            minute : d.getMinutes()
          };
          
          this.placeholder.sport = r.sport;
          this.placeholder.contact = r.contact;
          this.placeholder.note = r.note;
          this.placeholder.address = r.address;
          this.eventForm.setValue({date : date , time : time , address : r.address , note : r.note?r.note : '',contact : r.contact?r.contact:''})
        }
        else{
          this.err = res['result'];
        }
      })
    });
    let date = new Date;
    this.minDate.day = date.getDay();
    this.minDate.month = date.getMonth() +1;
    this.minDate.year = date.getFullYear();
  }

  ngOnInit() {
  }
  
  createForm(){
    this.eventForm = this.fb.group({
      date : [{},Validators.required],
      time : [{},Validators.required],
      address : ['',Validators.required],
      note : '',
      contact : '',
    });
  }

  onEventSubmit(){
    console.log('sdf');
    this.err = '';
    let date = this.eventForm.value.date;
    let time = this.eventForm.value.time;
    let obj = {
      date_time : new Date(date['year'],date['month']-1,date['day'],time['hour'],time['minute'],0),
      address : this.eventForm.value.address,
    };
    if(this.eventForm.value.note){
      obj['note']=this.eventForm.value.note;
    }
    if(this.eventForm.value.contact)
      obj['contact']=this.eventForm.value.contact;
    console.log(obj.date_time)
    if(this.eventForm.valid && obj.date_time > new Date())
    this.es.editMatch(this.id,obj)
    .subscribe(res => {
      if(res['status'] == 200){
        this.notif.getMatches();
        this.router.navigateByUrl('/events/'+res['result']);
      }
      else {
        this.err = res['result'];
      }
    });
    else if( obj.date_time <= new Date() ) this.err = 'please enter a valid date';
    else this.err = 'Please fill all fields';
  }
}
