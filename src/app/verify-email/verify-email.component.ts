import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {
  
  disp = '';
  err = '';
  uid = '';
  constructor(private login : LoginService , private route : ActivatedRoute) { 
    this.route.params.subscribe(param=>{
      this.uid = param['uid'];
    });
  }

  ngOnInit() {
    this.login.verifyEmail(this.uid)
      .subscribe(res=>{
        if(res['status'] == 200)
          this.disp = res['result'];
        else this.err = res['result'];
    });
  }
}
