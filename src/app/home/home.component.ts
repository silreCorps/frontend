import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service'; 
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

declare let FB : any;
declare let gapi : any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  loginForm : FormGroup;
  regForm : FormGroup;
  formError = '';
  authError = ''; 
  regError = '';
  regFormError = '';
  closeResult: string;
  modal : NgbModalRef;
  loading = false;
  GoogleAuth :any;
  
  constructor(private fb : FormBuilder , private loginService : LoginService , private user : UserService,private modalService: NgbModal,private router : Router ) { 
    this.createForm();
    gapi.load('auth2',()=>{
      gapi.auth2.init({ 
        client_id : "441852936408-1h2t5aue33cadlg5vbks06lum6kccglr.apps.googleusercontent.com",
        fetch_basic_profile : true,
      });
      this.GoogleAuth = gapi.auth2.getAuthInstance();
    });
  }
  
  ngOnInit() { 
    this.user.getUserStatus().subscribe(value => {
      if(value) {
        this.router.navigateByUrl('/home');
      }
    });
  } 

  createForm(){
    this.loginForm = this.fb.group({
      email : ['',Validators.email],
      password : ['',Validators.required],
    });
    this.regForm = this.fb.group({
      email : ['',Validators.email],
      first_name : ['',Validators.required],
      last_name : '',
      password : ['',Validators.required],
      confirm_password : ['',Validators.required],
    });
  }

  facebook(){
    FB.login((response)=>{
      if (response.authResponse) {
      let authResponse = response.authResponse;
      FB.api(`/me?fields=name,email,picture,first_name,last_name`, (fbUser: any) => {
        let obj = {
          idToken : authResponse.userID,
          first_name : fbUser.first_name,
          last_name : fbUser.last_name,
          email : fbUser.email, 
        };
        let a = this.loginService.facebookAuth(obj)
        .subscribe(res=>{
          if(res['status']==200){
            this.user.setUserStatus(true);
            this.router.navigateByUrl('/home');
            a.unsubscribe();
          }
        });
        });
      } 
      else {
        
      }
    },{scope : 'public_profile,email'});
  }

  googleLogin(state){
    if(state){
      let user = this.GoogleAuth.currentUser.get();
      let profile = user.getBasicProfile();
      //console.log(profile);
      let obj={
        first_name : profile.getGivenName(),
        last_name : profile.getFamilyName(),
        email : profile.getEmail(),
        idToken : user.getAuthResponse().id_token,
      };
      let b = this.loginService.googleAuth(obj)
      .subscribe((res)=>{
        if(res['status']==200){
          this.user.setUserStatus(true);
          this.router.navigateByUrl('/home');
        }
        b.unsubscribe();
      });
    }
    else{
      this.GoogleAuth.signIn()
      .then(user => {
        //console.log(user);
        let obj={
          first_name : user.name,
          last_name : user.family_name,
          email : user.email,
          idToken : gapi.auth2.AuthResponse.id_token,
        };
        let a = this.loginService.googleAuth(obj)
        .subscribe((res)=>{
          if(res['status']==200){
            this.router.navigateByUrl('/home');
          }
          a.unsubscribe();
        });
      })
      .catch(err => {
        //console.log(err);
      })
    }
  }

  google(){
    
    this.GoogleAuth.isSignedIn.listen((state)=>{
      this.googleLogin(state);
    });
    this.googleLogin(this.GoogleAuth.isSignedIn.get());
    
  }

  onSubmit(){
    this.formError = '';
    this.authError = '';
    if(this.loginForm.valid){
      this.loading = true;
      let login={
        email : this.loginForm.value.email,
        password : this.loginForm.value.password,
      }
      let a = this.loginService.login(login)
      .subscribe((res)=>{
        if(res['status']==200){
          if(this.modal) this.modal.close();
          this.user.setUserStatus(true);
          this.router.navigateByUrl('/home');
        }
        else {
          this.loading = false;
          this.authError = res['result'];
        }
        a.unsubscribe();
      });
    }
    else{
      if(!this.loginForm.get('email').valid)
        this.formError = 'Please enter valid email address';
      else if(!this.loginForm.get('password').valid)
        this.formError = "please enter a password";
    }
  }

  onRegSubmit(){
    this.regFormError = '';
    this.regError = '';
    if(this.regForm.valid&&this.regForm.value.password == this.regForm.value.confirm_password &&this.regForm.value.password.length > 6){
      let login={
        email : this.regForm.value.email,
        first_name : this.regForm.value.first_name,
        last_name : this.regForm.value.last_name,
        password : this.regForm.value.password,
      }; 
      let a = this.loginService.Register(login)
      .subscribe((res)=>{
        if(res['status']==200){
          this.modal.close();
          this.user.setUserStatus(true);
          this.router.navigateByUrl('/editprofile');
        }
        else{
          this.regError = res['result'];
        }
        a.unsubscribe(); 
      });
    }
    else{
      if(!this.regForm.valid){
        this.regError = 'Please fill all the fields correctly';
      }
      else if (this.regForm.value.password.length <= 6){
        this.regFormError = 'The password should be atleast 7 characters long';
      }
      else{
        this.regFormError = 'password and confirm password does not match';
      }
    }
  }

  open(content) {
    this.modal = this.modalService.open(content);
  }

}
