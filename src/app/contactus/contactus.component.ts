import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  constructor(private user : UserService) {
    this.userStatus = this.user.getUserStatus();
   }
  userStatus;
  ngOnInit() {
  }

}
