import { Routes } from '@angular/router';
import { FeedComponent } from '../feed/feed.component';
import { HomeComponent } from '../home/home.component';
import { ProfileComponent } from '../profile/profile.component';
import { CreateNewEventComponent } from '../create-new-event/create-new-event.component';
import { NotFoundComponent } from '../not-found/not-found.component';
import { EventComponent } from '../event/event.component';
import { EditprofileComponent } from '../editprofile/editprofile.component';
import { ForgotpasswordComponent } from '../forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from '../resetpassword/resetpassword.component';
import { RecoveraccountComponent } from '../recoveraccount/recoveraccount.component';
import { ContactusComponent } from '../contactus/contactus.component';
import { VerifyEmailComponent } from '../verify-email/verify-email.component';
import { TermsAndConditionsComponent } from '../terms-and-conditions/terms-and-conditions.component';
import { PrivacyPolicyComponent } from '../privacy-policy/privacy-policy.component';
import { MessengerComponent } from '../messenger/messenger.component';
import { EditMatchComponent } from '../edit-match/edit-match.component';
import { JoinedMatchesComponent } from '../joined-matches/joined-matches.component';
import { NotificationComponent } from '../notification/notification.component';

export const routes:Routes=[
    { path : '' , component : HomeComponent },
    { path : 'login' , component : HomeComponent },
    { path : 'home' , component : FeedComponent },
    { path : 'profile/:email' , component : ProfileComponent },
    { path : 'create_new_event' , component : CreateNewEventComponent },
    { path : 'events/:id' , component : EventComponent},
    { path : 'edit/:id' , component : EditMatchComponent},
    { path : 'editprofile' , component : EditprofileComponent},
    { path : 'forgot_password', component : ForgotpasswordComponent},
    { path : 'reset_password', component : ResetpasswordComponent},
    { path : 'recoverpwd/:uid' , component : RecoveraccountComponent},
    { path : 'verify/email/:uid' , component : VerifyEmailComponent},
    { path : 'contact' , component : ContactusComponent},
    { path : 'termsandconditions',component : TermsAndConditionsComponent},
    { path : 'privacypolicy', component : PrivacyPolicyComponent},
    { path : 'messenger' , component : MessengerComponent},
    { path : "my_matches" , component : JoinedMatchesComponent},
    { path : "notification" , component : NotificationComponent},
    { path : '**' , component : NotFoundComponent }
  ];