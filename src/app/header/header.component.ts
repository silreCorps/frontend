import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { NotificationService } from '../services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  email = '';
  constructor(public user : UserService , public notification : NotificationService , private router : Router) {
  }
  userStatus = this.user.getUserStatus();
  ngOnInit() {
    this.email = this.user.getEmail();
  }
    
  logout(){
    this.user.logout();
  }
}

