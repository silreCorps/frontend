import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  constructor(private user : UserService , private router : Router) { }

  notifications = [];
  err = "";

  ngOnInit() {
    this.user.getUserStatus().subscribe(res=>{
      if(!res){
        this.router.navigateByUrl('home');
      }
    });
    this.user.getNotification().subscribe(res=>{
      console.log(res);
      if(res['status']==200){
        this.notifications = res['result'];
      }
      else this.err = "Some error occured try again later";
    })
  }

  navigate(n){
    if(n.url=='profile2'){
      this.router.navigateByUrl('profile/'+n.generator);
    }
    else if(n.url=='match'){
      this.router.navigateByUrl('events/'+n._id);
    }
  }

}
