import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  reset : FormGroup;
  err = '';
  err1 = "";
  err2 = "";
  err3 = "";
  disp = '';
  constructor(private fb:FormBuilder , private login : LoginService , private user : UserService , private router : Router)
  { 
    this.user.getUserStatus().subscribe(value => {
      if(!value) {
        this.router.navigateByUrl('');
      }
    });
    this.createForm();
  }

  createForm(){
    this.reset = this.fb.group({
      curnt_pwd : ['',Validators.required],
      pwd : ['',Validators.required],
      cnfrm_pwd : ['',Validators.required],
    });
  }

  ngOnInit() {
  }

  onSubmit(){
    this.err = "";
    this.err1 = "";
    this.err2 = "";
    this.err3 = "";
    if(this.reset.valid && this.reset.value.pwd == this.reset.value.cnfrm_pwd){
      let obj = {
        current_password : this.reset.value.curnt_pwd,
        password : this.reset.value.pwd,
      };
      this.login.changePassword(obj)
      .subscribe(res =>{
        if(res['status'] == 200){
          this.disp = 'Password changed successfuly';
        }
        else{
          this.err = res['result'];
        }
      });
    }
    else{
      let val = this.reset.value;
      if(val.curnt_pwd.length == 0) this.err1 = "Current Password is required";
      if(val.pwd.length == 0) this.err2 = "New Password is required";
      if(val.cnfrm_pwd.length == 0) this.err3 = "Confirm Password is required";
    }
  }
}
