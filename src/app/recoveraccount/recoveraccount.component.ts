import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recoveraccount',
  templateUrl: './recoveraccount.component.html',
  styleUrls: ['./recoveraccount.component.css']
})
export class RecoveraccountComponent implements OnInit {

  reset : FormGroup;
  uid : '';
  disp = '';
  err = '';

  constructor(private fb:FormBuilder , private login : LoginService , private route : ActivatedRoute) {
    this.route.params.subscribe(param=>{
      this.uid = param['uid'];
    });
    this.createForm();
   }

  createForm(){
    this.reset = this.fb.group({
      pwd : ['',Validators.required],
      cnfrm_pwd : ['',Validators.required],
    });
  }

  ngOnInit() {
  }

  onSubmit(){
    if(this.reset.valid &&  this.reset.value.pwd == this.reset.value.cnfrm_pwd){
      let obj = {
        password : this.reset.value.pwd,
      }
      this.login.resetPassword(obj,this.uid)
      .subscribe(res => {
        if(res['status']==200){
          this.disp = "password reset sucessful";
        }
        else{
          this.err = res['result'];
        }
      });
    }    
  }

}
