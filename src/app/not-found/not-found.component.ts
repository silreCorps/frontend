import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {
  userStatus;
  constructor(private user : UserService) { 
    this.userStatus = this.user.getUserStatus();
  }

  ngOnInit() {
  }

}
