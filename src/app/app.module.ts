import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule , MatButtonModule } from '@angular/material'
import "hammerjs";
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TimeAgoPipe } from 'time-ago-pipe';
import { GtagModule } from 'angular-gtag';
import {IconUser , IconPlus , IconStar , IconBell , IconHome} from 'angular-feather';

import { RoutingModule } from './routing/routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FeedComponent } from'./feed/feed.component';
import { CreateNewEventComponent } from './create-new-event/create-new-event.component';
import { ProfileComponent } from './profile/profile.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UserService } from './services/user.service';
import { LoginService } from './services/login.service';
import { FeedService } from './services/feed.service';
import { EventsService } from './services/events.service';
import { NotificationService } from './services/notification.service';
import { EventComponent } from './event/event.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { RecoveraccountComponent } from './recoveraccount/recoveraccount.component';
import { ContactusComponent } from './contactus/contactus.component';
import { ShareButtonModule } from '@ngx-share/button';
import { FooterComponent } from './footer/footer.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { MessengerComponent } from './messenger/messenger.component';
import { EditMatchComponent } from './edit-match/edit-match.component';
import { FirebaseNotificationsService } from './services/firebase-notifications.service';
import { NotificationComponent } from './notification/notification.component';
import { JoinedMatchesComponent } from './joined-matches/joined-matches.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    CreateNewEventComponent,
    ProfileComponent,
    NotFoundComponent,
    FeedComponent,
    EventComponent,
    EditprofileComponent,
    ResetpasswordComponent,
    ForgotpasswordComponent,
    RecoveraccountComponent,
    ContactusComponent,
    TimeAgoPipe,
    FooterComponent,
    VerifyEmailComponent,
    PrivacyPolicyComponent,
    TermsAndConditionsComponent,
    MessengerComponent,
    EditMatchComponent,
    NotificationComponent,
    JoinedMatchesComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,BrowserAnimationsModule,FormsModule,ReactiveFormsModule,NgbModule.forRoot(),
    RoutingModule,MatProgressSpinnerModule,MatButtonModule,ShareButtonModule.forRoot(),IconPlus , IconStar , IconUser,IconBell , IconHome , 
    GtagModule.forRoot({ trackingId: 'UA-124176838-1', trackPageviews: true }),
  ],
  providers: [UserService,LoginService,CookieService,FeedService,EventsService,NotificationService,FirebaseNotificationsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
