import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { url } from '../shared/url';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class FeedService {
  constructor(private http : HttpClient , private user : UserService) { }
  getfeed(){
    let header = new HttpHeaders({'Authorization': "bearer "+this.user.getToken()});
    return this.http.get(url+'/event/getallevents',{headers : header});    
  }
  getFilteredFeed(latitude?,longitude?,sport?){
    let header = new HttpHeaders({'Authorization': "bearer "+this.user.getToken()});
    let query;
    return this.http.get(url+'/event/getfilteredevents?latitude='+latitude+'&longitude='+longitude+'&sport='+sport,{headers : header});    
  }
}
