import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {url} from '../shared/url';
import { Router } from '@angular/router';
import { NotificationService } from './notification.service';

@Injectable()
export class UserService {

  constructor(private cookie : CookieService , private http : HttpClient , private router : Router , private notif : NotificationService) { }

  // variable keeping data if user is loged in or not

//  userLogedin = new BehaviorSubject<boolean> (this.cookie.check('token'));

  userLogedin = new BehaviorSubject<boolean> (this.checkToken());
  userEmail = new BehaviorSubject<string> (this.cookie.get('email'));

  getUserStatus(){
    return this.userLogedin.asObservable();
  }
  // function to change state of website
  
  setUserStatus(bool){
    this.userLogedin.next(bool);
  }

  getToken(){
    return this.cookie.get('token');
  }

  checkToken(){
    return this.cookie.check('token');
  }

  logout(){
    let header = new HttpHeaders({'authorization' : "bearer "+ this.getToken()});
    this.http.get(url+'/users/logout',{headers : header,withCredentials : true})
    .subscribe((res)=>{
      this.cookie.deleteAll();
      this.notif.disconnectSocket();
      this.userLogedin.next(false);
      this.router.navigateByUrl('/login');
    });
  }

  getProfile(email){
    let header = new HttpHeaders({'authorization' : "bearer "+ this.getToken()});
    return this.http.post(url+'/users/getprofile/',{email : email} , {headers : header});
  }
  
  getUpcoming(){
    let header = new HttpHeaders({'authorization' : "bearer "+ this.getToken()});
    return this.http.get(url+'/users/upcomingevents', {headers : header});
  }

  getEmail(){
    return this.cookie.get('email');
  }

  updateProfileDetails(obj){
    let header = new HttpHeaders({'authorization' : 'bearer '+this.getToken()});
    return this.http.post(url+'/users/editprofile',obj,{headers : header});
  }

  updateProfilePicture(pic : File){
    const formData: FormData = new FormData();
    formData.append('imageFile', pic);
    let header = new HttpHeaders({ 'authorization' : 'bearer '+this.getToken()});
    return this.http.post(url+'/users/updateprofilepicture',formData,{headers : header});
  }

  email(){
    return this.cookie.get('email');
  }

  getName(){
    return this.cookie.get('name');
  }

  addFriend(email){
    let header = new HttpHeaders({"Authorization" : "Bearer "+this.cookie.get('token')});
    return this.http.get(url+'/users/addfriend/'+email,{headers : header});
  }

  undoRequest(email){
    let header = new HttpHeaders({"Authorization" : "Bearer "+this.cookie.get('token')});
    return this.http.get(url+'/users/undorequest/'+email,{headers : header});
  }

  unfriend(email){
    let header = new HttpHeaders({"Authorization" : "Bearer "+this.cookie.get('token')});
    return this.http.get(url+'/users/unfriend/'+email,{headers : header});
  }

  rejectRequest(email){
    let header = new HttpHeaders({"Authorization" : "Bearer "+this.cookie.get('token')});
    return this.http.get(url+'/users/rejectrequest/'+email,{headers : header});
  }

  acceptRequest(email){
    let header = new HttpHeaders({"Authorization" : "Bearer "+this.cookie.get('token')});
    return this.http.get(url+'/users/acceptrequest/'+email,{headers : header});
  }

  getfriends(){
    let header = new HttpHeaders({"Authorization" : "Bearer "+this.cookie.get('token')});
    return this.http.get(url+'/users/getfriends',{headers : header});  
  }

  getNotification(){
    let header = new HttpHeaders({authorization : 'Bearer '+this.cookie.get('token')});
    return this.http.get(url+'/users/getnotification',{headers : header});
  }

}
