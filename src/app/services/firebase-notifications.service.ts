import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { url } from '../shared/url';
import { CookieService } from 'ngx-cookie-service';

declare let firebase : any; 
@Injectable({
  providedIn: 'root'
})
export class FirebaseNotificationsService {
  messaging = firebase.messaging();
  constructor(private http: HttpClient , private cookie  : CookieService) {
    this.listner(); 
  }
  listner(){
    console.log(this.messaging);
    this.messaging.usePublicVapidKey("BKATl2hLFJnYdNr8lQVhNbPONRXHkLaGTWI5KRC-QuLyFZEncjxCEYr9GOo_9ifU19rltrMmzWE56YKZO_NaPqA");
    this.messaging.requestPermission()
    .then(()=>{
      console.log('Notification permission granted.');
      return this.messaging.getToken()
      .then(function(currentToken) {
        if (currentToken) {
          console.log(currentToken);
          return currentToken;
        } else {
          console.log('no permission');
        }
      })
      .then(token=>{
        if(token)
        this.sendToken(token);
      })
      .catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
      });
    })
    .catch(function(err) {
      console.log('Unable to get permission to notify.', err);
    });
    this.messaging.onTokenRefresh(function() {
      this.messaging.getToken().then(function(refreshedToken) {
        console.log('Token refreshed.');
        this.sendToken(refreshedToken);
      }).catch(function(err) {
        console.log('Unable to retrieve refreshed token ', err);
      });
    });
    
  }

  sendToken(token){
    let header = new HttpHeaders({'Authorization' : 'bearer '+this.cookie.get('token')});
    this.http.post(url+'/users/fcm_token',{token : token},{headers : header})
    .subscribe(res=>{
      if(res['status']==200){
        console.log('success');
      }
      else{
        console.log('bad luck');
      }
    });
  }
}
