import { TestBed, inject } from '@angular/core/testing';

import { FirebaseNotificationsService } from './firebase-notifications.service';

describe('FirebaseNotificationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirebaseNotificationsService]
    });
  });

  it('should be created', inject([FirebaseNotificationsService], (service: FirebaseNotificationsService) => {
    expect(service).toBeTruthy();
  }));
});
