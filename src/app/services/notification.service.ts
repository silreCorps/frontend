import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { BehaviorSubject } from 'rxjs';
import { url } from '../shared/url';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 

interface measg{
  content : String,
  email : String,
  time : Date,
}

interface match{
  _id : String,
  image : String,
  sport : String,
  address : String,
  date_time : Date,
  note : String,
  messages : [measg],
}


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private cookie : CookieService , private http : HttpClient) { }

  example : [match];
  message : measg;
  matches = new BehaviorSubject<[match]>(this.example);
  socket;
  id = new BehaviorSubject<String>('');

  initializeSocket(){
    this.socket = io(url,{
      path : '/messages',
      query : {
        token : this.cookie.get('token'),
        email : this.cookie.get('email'),
      },
    });
    this.socket.on('connect',()=>{
      this.getIcomingMessages();
    })
  }

  getIcomingMessages(){
    this.socket.on('incoming message',(obj)=>{
      let message = {
        content : obj.content,
        time : obj.time,
        email : obj.email,
        name : obj.name
      }
      this.example.find((ele)=>ele._id == obj._id).messages.push(message);
      this.matches.next(this.example);
      console.log('ok');
    });
  }

  sendMessage(obj,id){
    let message ={
      content : obj.content,
      time : obj.time,
      email : obj.email,
      _id : id,
      name : obj.name,
    }
    this.example.find(ele => ele._id == id).messages.push(obj);
    this.socket.emit('new message',message);
  }

  getMatches(){
    let header = new HttpHeaders({authorization : 'Bearer '+this.cookie.get('token')});
    this.http.get(url+'/users/upcomingevents',{headers : header}).subscribe(res=>{
      if(res['status']==200){
        this.example = res['result'];
        this.matches.next(this.example);
      }
    })
  }

  getMessagesById(id){
    let arr = [];
    this.example.find(ele => {
      if(ele._id == id){
        arr = ele.messages;
        return true;
      }
      return false;
    });
    return arr; 
  }

  getId(){
    return this.id.asObservable();
  }

  setId(id){
    this.id.next(id);
  }

  disconnectSocket(){
    this.socket.disconnect();
    this.socket.removeAllListeners();
  }

  leave(id){
    this.socket.emit('leave',id);
  }

  join(id){
    this.socket.emit('join',id);
  }

  getAllMessages(){
    return this.matches.asObservable();
  }

}
