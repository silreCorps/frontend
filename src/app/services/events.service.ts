import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { url } from '../shared/url';
import { LoginService } from './login.service';
import { UserService } from './user.service';
import { NotificationService } from './notification.service';
@Injectable({
  providedIn: 'root'
})
export class EventsService {
  
  constructor(public http : HttpClient , public user : UserService , private notif : NotificationService) { }

  getEvent(id){
    let header = new HttpHeaders({'authorization' : 'Bearer '+this.user.getToken()});
    return this.http.get(url+'/event/getevent/'+id,{headers : header});
  } 
  
  createNewEvent(form){ 
    let header = new HttpHeaders({'content-type': 'application/json','authorization' : 'Bearer '+this.user.getToken()});
    return this.http.post(url+'/event/createevent',form,{headers : header});
  }

  sendInvites(obj){
    let header = new HttpHeaders({'authorization' : 'Bearer '+this.user.getToken()});
    this.http.post(url+'/event/sendinvites',obj,{headers : header}).subscribe(res=>{
    });
  }

  join(id){
    let header = new HttpHeaders({'authorization' : 'Bearer '+this.user.getToken()});
    return this.http.get(url+'/event/joinevent/'+id,{headers : header});
  }
  
  leave(id){
    this.notif.leave(id);
    let header = new HttpHeaders({'authorization' : 'Bearer '+this.user.getToken()});
    return this.http.get(url+'/event/leaveevent/'+id,{headers : header});
  }

  editMatch(id,obj){
    let header = new HttpHeaders({'authorization' : 'Bearer '+this.user.getToken()});
    return this.http.post(url+'/event/editevent/'+id,obj,{headers : header});
  }
}
