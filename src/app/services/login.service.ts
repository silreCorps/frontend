import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {url} from '../shared/url';
import { UserService } from './user.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LoginService {

  constructor( private http : HttpClient , private user : UserService , private cookie : CookieService) { }
  
  // function to send login request

  login(form){
    return this.http.post(url+'/users/login',form,{withCredentials : true});
  }

  // function to send registration request

  Register(form){
    return this.http.post(url+'/users/register',form,{withCredentials : true});
  }
  
  // function to change passwrod from with in profile

  changePassword(obj){
    let header = new HttpHeaders({'Authorization':'bearer '+this.user.getToken()});
    return this.http.post(url+'/reset/password',obj,{headers : header});
  }

  // if user forgot the password

  forgotPassword(email){
    return this.http.get(url+'/reset/password/'+email);
  }

  // to reset password

  resetPassword(obj,uid){
    return this.http.post(url+'/reset/change_password/'+uid,obj);
  }

  // facebbok login/register

  facebookAuth(obj){
    return this.http.post(url+'/users/oauth2/facebook',obj,{withCredentials : true});
  }

  //google login/register

  googleAuth(obj){
    return this.http.post(url+'/users/oauth2/google',obj,{withCredentials : true});
  }

  //verify email account

  verifyEmail(id){
    return this.http.get(url+'/users/verify/'+id);
  }
  
}
