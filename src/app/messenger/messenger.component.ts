import { Component, OnInit , ViewChild , ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NotificationService } from '../services/notification.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-messenger',
  templateUrl: './messenger.component.html',
  styleUrls: ['./messenger.component.css']
})
export class MessengerComponent implements OnInit {

  @ViewChild('messenger') private scrollContainer : ElementRef;
  email='';
  messages = [];
  msg = new FormControl('');
  id;
  name='';
  constructor(private notif : NotificationService , private user : UserService , private router : Router) {
    this.notif.getId().subscribe(id=>{
      if(id){
        this.id=id;
        this.messages = this.notif.getMessagesById(id);
      }
      else{
        this.router.navigateByUrl('/home');
      }
    });
    this.user.getUserStatus().subscribe(val=>{
      if(!val){
        this.router.navigateByUrl('/home');
      }
    });
    this.email = this.user.email();
    this.name = this.user.getName();  
  }
  
  ngOnInit() {
    this.scrollToBottom();        
  }

  close(){
    this.notif.setId('');
  }

  send(){
    let nw={
      content : this.msg.value,
      time : new Date(),
      email : this.email,
      name : this.name,
    }
    // this.messages.push(nw);
    this.notif.sendMessage(nw,this.id);
    this.msg.reset('');
  }

  scrollToBottom(): void {
    try {
        this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;
    } catch(err) { }                 
  }
}
