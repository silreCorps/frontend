import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user.service';
import { NotificationService } from '../services/notification.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-joined-matches',
  templateUrl: './joined-matches.component.html',
  styleUrls: ['./joined-matches.component.css']
})
export class JoinedMatchesComponent implements OnInit {

  constructor(private user : UserService , private router : Router , private notif : NotificationService) { }
  
  events = [];
  err = "";
  loading =  false;
  
  ngOnInit() {
    this.loading = true;
    this.user.getUpcoming().subscribe(res=>{
      console.log(res);
      if(res['status']==200)
        this.events = res['result'];
      else this.err = "Some internal server error occured please try again later";
      this.loading = false;
    });
  }
  
  msg(id){
    this.notif.setId(id);
    this.router.navigateByUrl('/messenger');
  }
  
  mesag(id){
    this.notif.setId(id);
  }

  navigate(id){
    this.router.navigateByUrl('events/'+id);
  }

}
