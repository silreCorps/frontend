import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinedMatchesComponent } from './joined-matches.component';

describe('JoinedMatchesComponent', () => {
  let component: JoinedMatchesComponent;
  let fixture: ComponentFixture<JoinedMatchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinedMatchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinedMatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
