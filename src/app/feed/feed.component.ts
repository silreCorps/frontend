import { Component, OnInit } from '@angular/core';
import { FeedService } from "../services/feed.service";
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

const sports = ['Badminton','Tennis','Cricket','Volleyball','Basketball','Football'
,'Table Tennis','Pool','Chess','Kabaddi','Kho Kho','Hide and Seek','Hockey','Baseball',];

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  feeds=[];
  err = '';
  sport = new FormControl('');
  public model : any;
  check = false;
  loc = [];
  isLoading = false;
  radius = "";
  email;
  
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 1 ? []
        : sports.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  constructor(public fs : FeedService , private user : UserService , private router : Router ) {
  }
  
  ngOnInit() {
    this.user.getUserStatus().subscribe(value => {
      if(!value) {
        this.router.navigateByUrl('/');
      }
    });
    this.feed();
    this.email = this.user.email();
  }

  
  feed(){
    this.sport.reset('');
     this.fs.getfeed()
    .subscribe(res => {
      this.isLoading = false;
      if(res['status'] == 200){
        this.err='';
        this.feeds = res['result'];
        this.feeds.forEach(ele => {
          if(ele['participants'].filter(ele => ele.email == this.email)){
            this.feeds['show']=0;
          }
          else this.feeds['show']=1;
        });
      }
      else {
        this.err = res['result'];
      }
    });
  }

  filter(){
    let a = this.fs.getFilteredFeed(this.loc[0],this.loc[1],this.sport.value)
    .subscribe(res => {
      this.isLoading = false;
      if(res['status'] == 200){
        this.feeds = res['result'];
        this.feeds.forEach(ele => {
          if(ele['participants'].filter(ele => ele.email == this.email)){
            this.feeds['show']=0;
          }
          else this.feeds['show']=1;
        });
        this.err='';
      }
      else {
        this.err = res['result'];
      }
    });
  }

  // getloc(){
  //   if(navigator.geolocation&&!this.check){
  //     var options ={
  //       enableHighAccuracy : true
  //     }
  //     navigator.geolocation.getCurrentPosition(
  //       (pos)=> {
  //         this.loc = [pos.coords.latitude,pos.coords.longitude];
  //         this.check = true;   
  //       },
  //       (err)=>{
  //         this.check = false;    
  //       },
  //       options);
  //   }
  //   else {
  //     this.check = false;
  //     this.loc = [];
  //   }
  // }

  event(id){
    this.router.navigateByUrl('events/'+id);
  }

}
