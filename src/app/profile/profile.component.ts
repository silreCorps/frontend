import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';


//when you navigate to a profile who sent you request it doent show accept or decline , fix that

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  profile = {};
  err = '';
  isLoading = true;
  sport = [];
  toast = false;
  email;
  request_status = '';
  has_requested = false;

  constructor( public user : UserService , private router : Router , private route : ActivatedRoute) { 
    this.user.getUserStatus().subscribe(value => {
      if(!value) {
        this.router.navigateByUrl('');
      }
    });
    this.route.params.subscribe(param => {
      let email = param['email'];
      this.email = email;
      this.getprofile(email);
    });
  }

  getprofile(email){
    this.user.getProfile(email)
    .subscribe(res => {
      this.isLoading = false;
      if(res['status'] == 200 ){
        console.log(res['result']);
        this.profile = res['result'];
        this.sport = res['result'].events.reverse();
        if(!res['result'].has_sent_request)
          res['result'].requested ? this.request_status = 'Requested' : this.request_status = 'Add Friend';
        else {
          this.request_status = "Accept Request";
          this.has_requested = true;
        }
      }
      else{
        this.toast = true;
        setTimeout(()=>{this.toast=false},8000);
      }
    });
  }

  event(id){
    this.router.navigateByUrl('events/'+id);
  }

  ngOnInit() {
  }

  logout(){
    this.user.logout();
  }

  addFriend(){
    this.user.addFriend(this.email).subscribe( res => {
      if(res['status']==200){
        this.request_status = "Requested";
      }
      else{
        this.toast = true;
        setTimeout(()=>{this.toast=false},8000);
      }
    })
  }

  undoRequest(){
    this.user.undoRequest(this.email).subscribe( res => {
      if(res['status']==200){
        this.request_status = "Add Friend";
      }
      else{
        this.toast = true;
        setTimeout(()=>{this.toast=false},8000);
      }
    });
  }

  unfriend(email){
    this.user.unfriend(email).subscribe( res => {
      if(res['status']==200){
        this.getprofile(this.user.email());
      }
      else{
        this.toast = true;
        setTimeout(()=>{this.toast=false},8000);
      }
    });
  }

  rejectRequest(email){
    this.user.rejectRequest(email).subscribe( res => {
      if(res['status']==200){
        this.getprofile(this.user.email());
      }
      else{
        this.toast = true;
        setTimeout(()=>{this.toast=false},8000);
      }
    });
  }

  acceptRequest(email){
    this.user.acceptRequest(email).subscribe( res => {
      if(res['status']==200){
        this.getprofile(this.user.email());
      }
      else{
        this.toast = true;
        setTimeout(()=>{this.toast=false},8000);
      }
    });
  }
}
